# standard python
import os
import threading
import logging
import coloredlogs
import datetime
from argparse import ArgumentParser
import pyvisa
from astropy.time import Time

class LoggingClient:
    def __init__(self, ip, loglevel):
        self.ip = ip
        self.logger = get_colored_logger(self.__class__.__name__, loglevel)

        self.data_dir = os.getenv("DATA_DIR") or os.getenv("HOME") + "/data"

        if os.path.exists(self.data_dir):
            self.logger.debug(
                "Environment vars $DATA_DIR={}, $HOME={}".format(
                    os.getenv("DATA_DIR"), os.getenv("HOME")
                )
            )
            self.logger.info("Using data directory: {}".format(self.data_dir))
        else:
            self.logger.warning(
                "data directory {} does not exist.  Create now".format(self.data_dir)
            )
            os.mkdir(self.data_dir)

        self.filename_base = "tfr_" + datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")
        self.filename = self.data_dir + "/" + self.filename_base + ".csv"
        self.logger.info("Generated complete filepath: " + self.filename)

        # Flow control Events
        self.loop_stop_latch = threading.Event()
        self.logger.info(
            "TFR logging client instance created. ip={}, filename={}".format(self.ip, self.filename)
        )
        self.read_loop_timer = threading.Event()

    def stop_recv_loop(self):
        self.logger.debug("set read_loop_timer to continue the loop")
        self.read_loop_timer.set()
        try:
            self.logger.debug("close file")
            self.outfile.close()
        except Exception as e:
            self.logger.warning("{}".format(e))
            pass
        self.logger.debug("set loop_stop_latch to break out of recv_loop")
        self.loop_stop_latch.set()

    def start_recv_loop(self):
        comm_timeout_ms = 10e3
        self.logger.info("Connecting to {} ...".format(self.ip))

        # Use pure python VISA backend.  If not specifid, it attemps to find another other
        # VISA library from National Instruments, Keysight, ...
        self.mgr = pyvisa.ResourceManager("@py")

        # Use Python "with" context manager to garbage-collect and cleanup
        # file system and network device references if anything fails during this connection
        remote_device_path = "TCPIP0::{}::inst0::INSTR".format(self.ip)
        with self.mgr.open_resource(remote_device_path) as self.remote_device:
            id = self.remote_device.query("*IDN?")
            self.logger.info("connected {}".format(id))

            self.logger.debug("set communication timeout {}".format(comm_timeout_ms))
            self.remote_device.timeout = comm_timeout_ms

            self.logger.info("system preset ...")
            self.remote_device.write("SYST:PRES; *WAI")

            self.logger.info("clear error queue and device status ...")
            self.remote_device.write("*CLS; *WAI")

            self.logger.info("CH1 = PPS of GNSS SyncServer port J6")
            self.logger.info("CH2 = PPS distributor port 1 (origin H-Maser)")
            self.logger.info("Configuring device to measure CH1 - CH2")
            
            # Configure device to read time interval between ch 1 and 2
            self.logger.info("Set measurement type time interval")
            self.remote_device.write("CONF:TINT (@1), (@2); *WAI")

            # Set input coupling to DC for both inputs (measure DC voltage above 0V)
            self.logger.info("Set input coupling DC")
            self.remote_device.write("INP1:COUP DC; *WAI")
            self.remote_device.write("INP2:COUP DC; *WAI")
            
            # Set Impedance 50 ohms
            self.logger.info("Set input impedance 50 ohms")
            self.remote_device.write("INP1:IMP 50; *WAI")
            self.remote_device.write("INP2:IMP 50; *WAI")
            
            # Set Input voltage range
            self.logger.info("Set voltage range 5.0 V")
            self.remote_device.write("INP1:RANG 5.0; *WAI")
            self.remote_device.write("INP2:RANG 5.0; *WAI")

            # Set trigger threshold level in absolute volts (not percent of autoscale)
            self.logger.info("Set trigger level absolute 1.0 V")
            self.remote_device.write("INP1:LEVEL:ABS 1.0; *WAI")
            self.remote_device.write("INP2:LEVEL:ABS 1.0; *WAI")
            
            # Set to trigger on positive slope
            self.logger.info("Set trigger slope positive")
            self.remote_device.write("INP1:SLOP POS; *WAI")
            self.remote_device.write("INP2:SLOP POS; *WAI")
            
            # TODO (SS 04/2024) find more commands to arrive at the desired configuration 
            # that shows delay between PPS pulses.  We found a good configuration by playing
            # with front panel buttons, but now, I must reproduce it in this program -- how?
            
            self.logger.debug("clear flow control stop_latch for the logging loop")
            self.loop_stop_latch.clear()

            self.logger.debug("start recv_loop and block this thread until recv_loop is canceled")

            self.logger.info("Waiting for first data (timeout={})...".format(comm_timeout_ms))
            first_data_received = False

            # Loop forever until loop is canceled by user CTRL+C
            while not self.loop_stop_latch.is_set():
                
                self.logger.debug("Blocking read data from device ...")
                # Read result data from device
                data = self.remote_device.query("READ?")
                self.logger.debug("Device returned data")

                # Get timestamp from local computer immediately after data read
                time_now = Time.now()

                if first_data_received is False:
                    self.logger.info("Received first data")
                    first_data_received = True

                    self.logger.info("Opening file: {}".format(self.filename))
                    self.outfile = open(self.filename, "at")

                    headerline = "{},{},{},{}\n".format(
                        "time_iso",
                        "time_unix",
                        "time_mjd",
                        "pps_interval",
                    )
                    self.logger.warning(
                        "Writing data to file  {} ... CTRL+C to close file and disconnect".format(
                            self.filename
                        )
                    )
                    self.logger.debug(headerline[0:-1])
                    self.outfile.writelines([headerline])

                dataline = "{},{},{},{}\n".format(
                    time_now.isot,
                    time_now.unix,
                    time_now.mjd,
                    data.strip(),  # data string already includes a "\n" character from SCPI. use strip() to remove it
                )

                self.logger.debug(dataline[0:-1])
                self.outfile.writelines([dataline])

            self.logger.debug("break out of receive loop, clear device, close device")
            self.remote_device.clear()
            self.remote_device.close()


def get_colored_logger(name, log_level):
    fmt_scrn = "%(asctime)s.%(msecs)03d [%(levelname)s]%(message)s"

    level_styles_scrn = {
        "critical": {"color": "red", "bold": True},
        "debug": {"color": "white", "faint": True},
        "error": {"color": "red"},
        "info": {"color": "green", "bright": True},
        "notice": {"color": "magenta"},
        "spam": {"color": "green", "faint": True},
        "success": {"color": "green", "bold": True},
        "verbose": {"color": "blue"},
        "warning": {"color": "yellow", "bright": True, "bold": True},
    }
    field_styles_scrn = {
        "asctime": {},
        "hostname": {"color": "magenta"},
        "levelname": {"color": "cyan", "bright": True},
        "name": {"color": "blue", "bright": True},
        "programname": {"color": "cyan"},
    }
    formatter_screen = coloredlogs.ColoredFormatter(
        fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
    )

    # creating a handler to log on the console
    handler_screen = logging.StreamHandler()
    handler_screen.setFormatter(formatter_screen)

    logger = logging.getLogger(name)
    logger.addHandler(handler_screen)

    if log_level.upper() == "DEBUG":
        logger.setLevel(logging.DEBUG)
    elif log_level.upper() == "INFO":
        logger.setLevel(logging.INFO)
    elif log_level.upper() == "WARNING":
        logger.setLevel(logging.WARNING)
    elif log_level.upper() == "ERROR":
        logger.setLevel(logging.ERROR)
    else:
        logger.setLevel(logging.DEBUG)
        logger.error("invalid parameter log-level. Select level DEBUG")

    return logger


if __name__ == "__main__":
    # Process command line arguments
    parser = ArgumentParser(description="Receive status message record selected data to .csv file.")
    parser.add_argument(
        "-i", "--ip-address", dest="ip", type=str, default="192.168.90.26", help="Host IP address"
    )
    parser.add_argument(
        "-l",
        "--log-level",
        dest="loglevel",
        type=str,
        help="Log level for screen display",
        default="INFO",
    )

    args = parser.parse_args()
    logger = get_colored_logger("__main__", args.loglevel)

    logger.debug("test python logger in module {}".format(__name__))

    # Run the logger
    try:
        client = LoggingClient(args.ip, args.loglevel)
        client.start_recv_loop()
    except ConnectionRefusedError:
        logger.error("ConnectionRefusedError.  Cannot connect to {}".format(args.ip))
        pass
    except KeyboardInterrupt:
        logger.warning("Catch KeyboardInterrupt.  stop client")
        client.stop_recv_loop()
